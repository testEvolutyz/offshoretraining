//addew new line for tesing
package firstWebDriverpkg;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import junit.framework.Assert;

public class CalculaterIE {

	public WebDriver driver;
	
	public int ia=20000, md=2000, ai=5, y=5;
	@Before
	public void setup()
	{
		System.setProperty("java.net.preferIPv4Stack", "true");
		System.setProperty("webdriver.ie.driver","C:\\Users\\Srinivas\\Google Drive\\SeleniumTraining_2019\\IEDriverServer.exe");
		driver=new InternetExplorerDriver();
	}
	

	@Test
	public void testSavings()
	{
		driver.get("https://www.bankrate.com/calculators/savings/simple-savings-calculator.aspx");
		
		//initialAmount 
		WebElement initialAmount=driver.findElement(By.id("initialAmount"));
		//initialAmount.sendKeys("20000");
		initialAmount.sendKeys(Integer.toString(ia));
		System.out.println("Typed to initialAmount"+ia);
		
		//Monthly Deposit:
		WebElement MonthlyDeposit=driver.findElement(By.xpath("//html/body/div[5]/div[2]/div/div/div/div/div/div/div/div[2]/div/input"));
		//MonthlyDeposit.sendKeys("2000");
		MonthlyDeposit.sendKeys(Integer.toString(md));
		System.out.println("Typed to MonthlyDeposit"+md);
		
		
		//annual Interest:
		WebElement annualInterest=driver.findElement(By.xpath("//*[@id='annualInterest']"));
		//annualInterest.sendKeys("5");
		annualInterest.sendKeys(Integer.toString(ai));
		System.out.println("Typed to annualInterest"+ai);
		
		//Number of Years:
		WebElement years=driver.findElement(By.id("numberOfYears"));
		//years.sendKeys("5");
		years.sendKeys(Integer.toString(y));
		System.out.println("Typed to years"+y);
		
		
		//click calculate
		WebElement calculate=driver.findElement(By.xpath("//button[@id='calculate' and contains(@class, 'button')]"));
		calculate.click();
		System.out.println("Clicked on Calculate");
		
		
		//finalSavingsBalance
		WebElement finalSavingsBalance=driver.findElement(By.xpath("//*[@id='finalSavingsBalance']"));
		
		
		File screenshot=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File("Calculater.jpg") );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error in taking screenshot");
			e.printStackTrace();
		}
		
				
		String finalbalance=finalSavingsBalance.getText();
		System.out.println("Actaul final balance : "+finalbalance);
		
		
	
		DecimalFormat moneyFormat = new DecimalFormat("$0,000.00");
	
		
	
		
	}
	@After
	public void teardown()
	{
		driver.close();
	}
	
	
	public void DEMO()
	{
		//driver.close();
	}
	
public void DEMO2()
	{
		//driver.close();
	}
	
}
